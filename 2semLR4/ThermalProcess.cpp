#include "ThermalProcess.h"


ThermalProcess::ThermalProcess() {
	this->capacity1 = 0.0;
	this->capacity2 = 0.0;
	this->pressure = 0.0;
}

ThermalProcess::ThermalProcess(double capacity1, double capacity2, double pressure)
{
	this->capacity1 = capacity1;
	this->capacity2 = capacity2;
	this->pressure = pressure;
}

void ThermalProcess::print() {
	cout << "Operation= " << countoperation() << endl;
}

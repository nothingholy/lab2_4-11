#pragma once

#include <iostream>
#include <vector>
using namespace std;

class ThermalProcess {
protected:
	double capacity1; //����� ���������
	double capacity2; //�������� �����
	double pressure; //��������
public:
	ThermalProcess();
	ThermalProcess(double, double, double);
	virtual double countoperation() = 0;
	void print();
	friend istream& operator>>(istream& s, ThermalProcess& obj) {
		s >> obj.capacity1 >> obj.capacity2 >> obj.pressure;
		return s;
	}
};


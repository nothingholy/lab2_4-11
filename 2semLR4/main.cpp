#include "ThermalProcess.h"
#include "Isobaric.h"
#include "Isohoric.h"

int main() {
	vector<ThermalProcess*> A;
	while (true) {
		int k;
		cout << "Isohoric(1) or Isobaric(2) or exit(3)?"; cin >> k;
		switch (k)
		{
		case 1:
			int n;
			cout << "Enter quantity of elements: "; cin >> n;
			for (int i = 0; i < n; i++) {
				Isohoric* pIshoric = new Isohoric();
				cin >> *pIshoric;
				A.push_back(pIshoric);
				pIshoric->print();
			}
			break;
		case 2:
			int p;
			cout << "Enter quantity of elements: "; cin >> p;
			for (int i = 0; i < p; i++) {
				Isobaric* pIsobaric = new Isobaric();
				cin >> *pIsobaric;
				A.push_back(pIsobaric);
				pIsobaric->print();
			}
			break;
		case 3:
			return 0;
		default:
			cout << "Error!" << endl;
			break;
		}
	}


	system("pause");
	return 0;
}